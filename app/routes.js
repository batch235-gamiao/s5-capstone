const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		let foundAlias = exchangeRates.find((er) => {

		    return er.alias === req.body.alias

		});

		// 2
		if(!req.body.hasOwnProperty('name')){
	        return res.status(400).send({
	            'error' : 'Bad Request: missing required parameter NAME'
	        })
    	}

    	// 3
    	if(typeof req.body.name !== "string"){
    		return res.status(400).send({
    			'error' : 'Bad Input: name is not a STRING'
    		})
    	}

    	// 4
    	if(req.body.name.length === 0){
    		return res.status(400).send({
    			'error' : 'Bad Input: name is EMPTY'
    		})
    	}

    	// 5
    	if(!req.body.hasOwnProperty('ex')){
	        return res.status(400).send({
	            'error' : 'Bad Request: missing required parameter EX'
	        })
    	}

    	// 6
    	if(typeof req.body.ex !== "object"){
	        return res.status(400).send({
	            'error' : 'Bad Input: name is not an OBJECT'
	        })
    	}

    	// 7
    	if(Object.keys(req.body.ex).length === 0){
    		return res.status(400).send({
	            'error' : 'Bad Input: ex is EMPTY'
	        })
    	}

    	// 8
    	if(!req.body.hasOwnProperty('alias')){
	        return res.status(400).send({
	            'error' : 'Bad Request: missing required parameter ALIAS'
	        })
    	}

    	// 9
    	if(typeof req.body.alias !== "string"){
    		return res.status(400).send({
    			'error' : 'Bad Input: alias is not a STRING'
    		})
    	}

    	// 10
    	if(req.body.alias.length === 0){
    		return res.status(400).send({
    			'error' : 'Bad Input: alias is EMPTY'
    		})
    	}

    	// 11
    	if(foundAlias){
    		return res.status(400).send({
    			'error' : 'Bad Input: alias has duplicates'
    		})
    	}

    	// 12
    	else {    		
    		return res.status(200).send({
    			'success' : 'no error'
    		});
    	}    	
	})

module.exports = router;
