const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('Currency Test Suite', () => {
	
	/*it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);			
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})*/

	

	// 2
	it('Test Currency post if it returns 400 if no name property', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, "name is not missing");
            done();
        })
	})

	// 1
	it('Test API CURRENCY endpoint is running', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.notEqual(res.status, 404);
        })
	})

	// 3
	it('Test Currency post if it returns 400 if name is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
            name: 1,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'name is a string');
            done();
        })
	})

	// 4
	it('Test Currency post if it returns 400 if name is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
            name: '',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'name is not an empty string');
            done();
        })
	})

	// 5
	it('Test Currency post if it returns 400 if no ex property', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
            name: 'Shadowrun Nuyen'
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'ex is not missing');
            done();
        })
	})

	// 6
	it('Test Currency post if it returns 400 if ex property is not an object', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
            name: 'Shadowrun Nuyen',
            ex: 'not an object'
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'ex is an object');
            done();
        })
	})

	// 7
	it('Test Currency post if it returns 400 if ex property is an empty object', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 'nuyen',
            name: 'Shadowrun Nuyen',
            ex: {}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'ex is not an empty object');
            done();
        })
	})

	// 8
	it('Test Currency post if it returns 400 if no alias property', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            name: 'Shadowrun Nuyen',
            ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'alias is not missing');
            done();
        })
	})

	// 9 
	it('Test Currency post if it returns 400 if alias is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: 1,
            name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'alias is a string');
            done();
        })
	})

	// 10 
	it('Test Currency post if it returns 400 if alias is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: "",
            name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'alias is not an empty string');
            done();
        })
	})

	// 11 
	it('Test Currency post if it returns 400 if there are duplicate alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: "php",
            name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 400, 'alias has no duplicate');
            done();
        })
	})

	// 12
	it('Test Currency post if it returns 200', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            alias: "nuyen",
            name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
        })
        .end((err, res) => {
            assert.equal(res.status, 200, 'check if there is no duplicates or missing input');
            done();
        })
	})
})
